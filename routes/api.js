exports.api = function (params) {
    return function (req, res) {

        switch (req.params.resource) {
            case 'users':
                var qry = params.db('users');

                qry.where('email', req.body.email)
                    .exec(function (err, users) {

                        console.log("Pass" + req.body);
                        console.log("");

                        var hash = null;
                        if (req.body.password) {
                            // Hash password using bcrypt
                            var bcrypt = require('bcryptjs');
                            var salt = bcrypt.genSaltSync(10);
                            hash = bcrypt.hashSync(req.body.password, salt);
                        }

                        if (users[0]) {
                            var info = {
                                Name: req.body.name,
                                Role: req.body.role
                            };

                            if (req.body.password !== '') {
                                info.Password = hash;
                            }

                            params.db('users').update(info)
                                .where('email', req.body.email)
                                .exec(function () {
                                    res.json({"msg": "User update success."});
                                });

                        } else {

                            params.db('users').insert({
                                Name: req.body.name,
                                Email: req.body.email,
                                Password: hash,
                                Role: req.body.role
                            }, 'id').exec(function (err, insertId) {
                                if (err !== null) {
                                    res.json(400, {"msg": err});
                                } else {
                                    res.json({"msg": "User add success, with id: " + insertId});
                                }
                            });
                        }

                    });
                break;

            case 'hosts':
                var qry = params.db('groups');
                qry.where('name', req.body.group)
                    .exec(function (err, groups) {
                        var idGroup = 0;
                        if (groups.length) {
                            idGroup = groups[0].idGroup;
                        }

                        var qry_hosts = params.db('hosts');

                        qry_hosts.where('Url', req.body.url)
                            .exec(function (err, hosts) {

                                if (hosts.length) {
                                    var info = {
                                        Name: req.body.name,
                                        idGroup: idGroup
                                    };

                                    params.db('hosts').update(info)
                                        .where('Url', req.body.url)
                                        .exec(function() {
                                            params.config.readHosts(params.db, function(){
                                                res.json({"msg": "Host update success"});
                                            });
                                        });

                                } else {

                                    params.db('hosts').insert({
                                        Name: req.body.name,
                                        Url: req.body.url,
                                        idGroup: idGroup
                                    }, 'idHost').exec(function (err, insertId) {
                                        params.config.readHosts(params.db, function () {
                                            if (err !== null) {
                                                res.json(400, {"msg": err});
                                                console.log(err);
                                            } else {
                                                res.json({"msg": "Host add success, with id: " + insertId});
                                            }
                                        });
                                    });
                                }

                            });
                    });
                break;

            case 'groups':
                var qry = params.db('groups');
                qry.where('name', req.body.name)
                    .exec(function (err, groups) {
                        if (!groups.length) {
                            params.db('groups').insert({
                                Name: req.body.name,
                            }, 'idGroup').exec(function (err, insertId) {
                                if (err !== null) {
                                    res.json(400, {"msg": err});
                                } else {
                                    res.json({"msg": "Group add success, with id: " + insertId});
                                }
                            });
                        } else {
                            res.json({"msg": "Group exists"});
                        }
                    });
                break;


        }
    };
};