ARG FROM="node:8.9.0-alpine"
FROM ${FROM}

ARG ARK_ENV="com"

ENV ARK_ENV=${ARK_ENV}

RUN apk --no-cache add --virtual native-deps g++ gcc libgcc libstdc++ linux-headers make python libc-dev git && \
    npm install express node-pre-gyp --global --verbose

# Install nodervisor from git
WORKDIR /opt/nodervisor

# Install npm packages for nodervisor
RUN git clone npm install

# Run npm start by default
CMD ["npm","start"]

